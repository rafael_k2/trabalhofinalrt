<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Sobre';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Este site serve para você desabafar e conversar com um profissional da área específica.
    </p>

    <code><?= __FILE__ ?></code>
</div>
