<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RtAluno */

$this->title = 'Criar Aluno';
$this->params['breadcrumbs'][] = ['label' => 'Rt Alunos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rt-aluno-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
