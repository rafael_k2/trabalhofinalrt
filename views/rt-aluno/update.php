<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RtAluno */

$this->title = 'Atualizar Alunos: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rt Alunos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rt-aluno-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
