<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RtMsgchatSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rt-msgchat-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'texto') ?>

    <?= $form->field($model, 'data') ?>

    <?= $form->field($model, 'horario') ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'aluno_id') ?>

    <?php // echo $form->field($model, 'funcionario_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Procurar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Resetar', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
