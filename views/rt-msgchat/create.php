<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RtMsgchat */

$this->title = 'Criar Msgchat';
$this->params['breadcrumbs'][] = ['label' => 'Rt Msgchats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rt-msgchat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
