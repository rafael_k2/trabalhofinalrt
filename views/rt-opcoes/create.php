<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RtOpcoes */

$this->title = 'Criar Opções';
$this->params['breadcrumbs'][] = ['label' => 'Rt Opcoes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rt-opcoes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
