<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RtRespostas */

$this->title = 'Atualizar Respostas: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Rt Respostas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rt-respostas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
