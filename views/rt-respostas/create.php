<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RtRespostas */

$this->title = 'Criar Respostas';
$this->params['breadcrumbs'][] = ['label' => 'Rt Respostas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rt-respostas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
