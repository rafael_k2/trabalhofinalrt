<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RtRespostasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Respostas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rt-respostas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Criar Respostas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'texto',
            'pergunta_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
