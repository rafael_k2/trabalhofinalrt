<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Rtaluno;


/* @var $this yii\web\View */
/* @var $model app\models\RtAgenda */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rt-agenda-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'data')->textInput(['type'=>'date']) ?>

    <?= $form->field($model, 'horario')->textInput() ?>

    <?= $form->field($model, 'aluno_id')->
       dropDownList(ArrayHelper::map(Rtaluno::find()
           ->orderBy('nome')
           ->all(),'id','nome'),
           ['prompt' => 'Selecione um aluno'] )
?>


    <?= $form->field($model, 'validação')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
