<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RtAgendaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rt-agenda-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'data')->textInput(['type'=>'date']) ?>

    <?= $form->field($model, 'horario') ?>

    <?= $form->field($model, 'aluno_id') ?>

    <?= $form->field($model, 'validação') ?>

    <div class="form-group">
        <?= Html::submitButton('Procurar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Resetar', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
