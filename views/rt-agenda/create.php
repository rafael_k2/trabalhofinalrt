<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RtAgenda */

$this->title = 'Criar Agenda';
$this->params['breadcrumbs'][] = ['label' => 'Rt Agendas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rt-agenda-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
