<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RtPergunta */

$this->title = 'Atualizar Pergunta: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Rt Perguntas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rt-pergunta-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
