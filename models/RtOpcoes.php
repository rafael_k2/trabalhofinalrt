<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rt_opcoes".
 *
 * @property int $ID
 * @property string $data
 * @property int $opcoes_id
 *
 * @property RtOpcoes $opcoes
 * @property RtOpcoes[] $rtOpcoes
 */
class RtOpcoes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rt_opcoes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data'], 'safe'],
            [['opcoes_id'], 'required'],
            [['opcoes_id'], 'integer'],
            [['opcoes_id'], 'exist', 'skipOnError' => true, 'targetClass' => RtOpcoes::className(), 'targetAttribute' => ['opcoes_id' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'data' => 'Data',
            'opcoes_id' => 'Opções',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOpcoes()
    {
        return $this->hasOne(RtOpcoes::className(), ['ID' => 'opcoes_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRtOpcoes()
    {
        return $this->hasMany(RtOpcoes::className(), ['opcoes_id' => 'ID']);
    }
}
