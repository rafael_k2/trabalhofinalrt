<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rt_respostas".
 *
 * @property int $ID
 * @property string $texto
 * @property int $pergunta_id
 *
 * @property RtPergunta $pergunta
 */
class RtRespostas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rt_respostas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pergunta_id'], 'required'],
            [['pergunta_id'], 'integer'],
            [['texto'], 'string', 'max' => 250],
            [['pergunta_id'], 'exist', 'skipOnError' => true, 'targetClass' => RtPergunta::className(), 'targetAttribute' => ['pergunta_id' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'texto' => 'Texto',
            'pergunta_id' => 'Pergunta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPergunta()
    {
        return $this->hasOne(RtPergunta::className(), ['ID' => 'pergunta_id']);
    }
}
