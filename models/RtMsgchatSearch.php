<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RtMsgchat;

/**
 * RtMsgchatSearch represents the model behind the search form of `app\models\RtMsgchat`.
 */
class RtMsgchatSearch extends RtMsgchat
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['texto', 'data', 'horario'], 'safe'],
            [['ID', 'aluno_id', 'funcionario_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RtMsgchat::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'data' => $this->data,
            'horario' => $this->horario,
            'ID' => $this->ID,
            'aluno_id' => $this->aluno_id,
            'funcionario_id' => $this->funcionario_id,
        ]);

        $query->andFilterWhere(['like', 'texto', $this->texto]);

        return $dataProvider;
    }
}
