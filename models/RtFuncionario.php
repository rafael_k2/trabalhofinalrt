<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rt_funcionario".
 *
 * @property int $id
 * @property string $nome
 * @property string $CPF
 *
 * @property RtMsgchat[] $rtMsgchats
 */
class RtFuncionario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rt_funcionario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'required'],
            [['nome'], 'string', 'max' => 30],
            [['CPF'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'CPF' => 'CPF',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRtMsgchats()
    {
        return $this->hasMany(RtMsgchat::className(), ['funcionario_id' => 'id']);
    }
}
