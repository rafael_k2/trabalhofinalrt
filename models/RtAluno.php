<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rt_aluno".
 *
 * @property string $nome
 * @property int $id
 */
class RtAluno extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rt_aluno';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'required'],
            [['nome'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nome' => 'Nome',
            'id' => 'ID',
        ];
    }
}
