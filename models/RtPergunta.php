<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rt_pergunta".
 *
 * @property int $ID
 * @property string $texto
 *
 * @property RtRespostas[] $rtRespostas
 */
class RtPergunta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rt_pergunta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['texto'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'texto' => 'Texto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRtRespostas()
    {
        return $this->hasMany(RtRespostas::className(), ['pergunta_id' => 'ID']);
    }
}
