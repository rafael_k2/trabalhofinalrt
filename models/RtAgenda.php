<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rt_agenda".
 *
 * @property int $id
 * @property string $data
 * @property string $horario
 * @property int $aluno_id
 * @property int $validação
 *
 * @property RtAluno $aluno
 */
class RtAgenda extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rt_agenda';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data', 'horario'], 'safe'],
            [['aluno_id', 'validação'], 'required'],
            [['aluno_id', 'validação'], 'integer'],
            [['aluno_id'], 'exist', 'skipOnError' => true, 'targetClass' => RtAluno::className(), 'targetAttribute' => ['aluno_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data' => 'Data',
            'horario' => 'Horário',
            'aluno_id' => 'Aluno',
            'validação' => 'Validação',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAluno()
    {
        return $this->hasOne(RtAluno::className(), ['id' => 'aluno_id']);
    }
}
