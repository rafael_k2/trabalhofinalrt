<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rt_msgchat".
 *
 * @property string $texto
 * @property string $data
 * @property string $horario
 * @property int $ID
 * @property int $aluno_id
 * @property int $funcionario_id
 *
 * @property RtAluno $aluno
 * @property RtFuncionario $funcionario
 */
class RtMsgchat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rt_msgchat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['texto', 'aluno_id', 'funcionario_id'], 'required'],
            [['data', 'horario'], 'safe'],
            [['aluno_id', 'funcionario_id'], 'integer'],
            [['texto'], 'string', 'max' => 230],
            [['aluno_id'], 'exist', 'skipOnError' => true, 'targetClass' => RtAluno::className(), 'targetAttribute' => ['aluno_id' => 'id']],
            [['funcionario_id'], 'exist', 'skipOnError' => true, 'targetClass' => RtFuncionario::className(), 'targetAttribute' => ['funcionario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'texto' => 'Texto',
            'data' => 'Data',
            'horario' => 'Horário',
            'ID' => 'ID',
            'aluno_id' => 'Aluno',
            'funcionario_id' => 'Funcionario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAluno()
    {
        return $this->hasOne(RtAluno::className(), ['id' => 'aluno_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFuncionario()
    {
        return $this->hasOne(RtFuncionario::className(), ['id' => 'funcionario_id']);
    }
}
